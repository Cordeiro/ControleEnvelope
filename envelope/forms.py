from django import forms
from .models import Curso, Professor, Status, Secretaria, Prova, Disciplina, Envelope
from django.contrib.admin.widgets import AdminDateWidget 
        
class CursoForm(forms.ModelForm):
    
    class Meta:
        model = Curso
        #fields = '__all__'
        exclude = ['nome',]
       

# Criacao da Tabela de Professores      
class ProfessorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfessorForm, self).__init__(*args, **kwargs)
        self.fields['nome'].widget.attrs['placeholder'] = 'Nome Completo'
        
    class Meta:
        model = Professor
        fields = ['nome','matricula',]
       

class StatusForm(forms.ModelForm):
    #def __init__(self, *args, **kwargs):
    #    super(StatusForm, self).__init__(*args, **kwargs)
    #    self.fields['status'].choices = 3
        
    class Meta:
        model = Status
        fields = '__all__'
       

class SecretariaForm(forms.ModelForm):
    
    class Meta:
        model = Secretaria
        exclude = ['secretaria_id',]
       

class ProvaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProvaForm, self).__init__(*args, **kwargs)
        self.fields['data_prova'].widget.attrs['placeholder'] = 'AAAA-MM-DD'
        self.fields['data_limite'].widget.attrs['placeholder'] = 'AAAA-MM-DD'
        
    class Meta:
        model = Prova
        exclude = ['status_id',]
       

class DisciplinaForm(forms.ModelForm):
    
    class Meta:
        model = Disciplina
        exclude = ['nome','prova',]
        
class EnvelopeForm(forms.ModelForm):
    
    class Meta:
        model = Envelope
        fields = '__all__'

class EditEnvelopeForm(forms.ModelForm):
    
    class Meta:
        model = Envelope
        fields = ['professor']
        
class ArticleForm(forms.Form):
    #cursos = Curso.objects.all()
    #for curso in cursos:
    #    nome = [curso.nome]
    title = forms.CharField()
    pub_date = forms.DateField()
    
class ProjectFilterForm(forms.Form):

    range = forms.ChoiceField(choices=[], required=False)

    def __init__(self, *args, **kwargs):
        super(ProjectFilterForm, self).__init__(*args, **kwargs)
        #oldest = Project.objects.first()
        #newest = Project.objects.last()
        #if oldest and newest:
        range_choices = []
        #for year in range(oldest.date.year, newest.date.year+1):
        #        range_choices.append('Spring {}'.format(year))
        #        range_choices.append('Fall {}'.format(year))
        self.fields['range'].choices = range_choices