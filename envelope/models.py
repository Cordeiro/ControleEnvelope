from django.db import models
from django.core import validators

# Criacao da Tabela de Curso
class Curso(models.Model):
    Nome_Turma = (
        ('A', 'A'),
        ('B', 'B'),
    )
    curso_id = models.AutoField(primary_key=True)
    nome = models.CharField('Curso',max_length=60, null=False)
    turma = models.CharField('Turma',max_length=1, null=True, choices=Nome_Turma)
    
    def __str__(self):
        return self.nome
    
    class Meta:
        db_table ='Cont_Curso'
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'
        ordering = ['nome'] 
    

class Status(models.Model):
    Num_Status = (
        (1, 'Entregue'),
        (2, 'Entregue Atrasado'),
        (3, 'Pendente'),
        (4, 'Atrasado'),
    )
    status_id = models.AutoField(primary_key=True)
    status = models.IntegerField ('Status', choices=Num_Status, default='3')
    data_devolucao = models.DateTimeField('Entregue em', auto_now=True)
    def __str__(self):
        return str(self.status)
        
    class Meta:
        db_table ='Cont_Status'
        verbose_name = 'Status'
        verbose_name_plural = 'Status'
        ordering = ['data_devolucao']
        
        
class Prova(models.Model):
    Num_Estagio = (
        ('1', '1º Estágio'),
        ('2', '2º Estágio'),
        ('3', '3º Estágio'),
    )
    prova_id = models.AutoField(primary_key=True)
    estagio = models.CharField('Estágio', max_length=1, choices=Num_Estagio)
    data_prova = models.DateField('Data da Prova', null=False) # Data pra realizar prova
    data_limite = models.DateField('Data Limite', null=False) # Data de entregar a prova dps de realizada
    status_id = models.OneToOneField(Status, on_delete=models.CASCADE) 
    def __str__(self):
        return str(self.data_prova)
    
    class Meta:
        db_table ='Cont_Prova'
        verbose_name = 'Prova'
        verbose_name_plural = 'Provas'
        ordering = ['-estagio','data_prova']
        

class Disciplina(models.Model):
    Num_Periodo = (
        ('1', '1º Período'),('2', '2º Período'),('3', '3º Período'),
        ('4', '4º Período'),('5', '5º Período'),('6', '6º Período'),
        ('7', '7º Período'),('8', '8º Período'),
    )
    disciplina_id = models.AutoField(primary_key=True, unique = False)
    nome = models.CharField('Disciplina', max_length=60, null=False)
    periodo = models.CharField('Período', max_length=1, choices=Num_Periodo)
    prova = models.ForeignKey(Prova, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.nome
        
    def periodo_str(self):
        return self.periodo
    
    class Meta:
        db_table ='Cont_Disciplina'
        verbose_name = 'Disciplina'
        verbose_name_plural = 'Disciplinas'
        ordering = ['nome']
        
# db_column = 'nome'
# Criacao da Tabela de Professores      
class Professor(models.Model):
    professor_id = models.AutoField(primary_key=True, unique = False)
    nome = models.CharField('Nome', max_length=80, unique=True, null=False)
    matricula = models.CharField('Matricula', max_length=15, unique=True, null=False, blank=False)
    disciplina = models.ForeignKey(Disciplina, on_delete=models.CASCADE)
    curso = models.ForeignKey(Curso)
    
    def __str__(self):
        return self.nome
        
    class Meta:
        db_table ='Cont_Professor'
        verbose_name = 'Professor'
        verbose_name_plural = 'Professores'
        ordering = ['nome']
        
                        
class Secretaria(models.Model):
    secretaria_id = models.AutoField(primary_key=True)
	#usuario_id = models.ForeignKey(User, on_delete = models.CASCADE)
    nome = models.CharField('Nome', max_length=80, null=False)
    cpf = models.CharField('CPF', max_length=14, unique=True)    
    
    def __str__(self):
        return self.nome
    
    class Meta:
        db_table ='Cont_Secretaria'
        verbose_name = 'Secretaria'
        verbose_name_plural = 'Secretarias'
        ordering = ['nome']
    
class Envelope (models.Model):
    envelope_id = models.AutoField(primary_key=True)
    professor = models.OneToOneField(Professor, unique = False)
    curso = models.OneToOneField(Curso, unique = False)
    disciplina = models.OneToOneField(Disciplina, unique = False)
    prova = models.OneToOneField(Prova)
    def __str__(self):
        return str(self.envelope_id)
    
    class Meta:
        db_table ='Cont_Envelopes'
        verbose_name = 'Envelope'
        verbose_name_plural = 'Envelopes'
        ordering = ['prova']
    