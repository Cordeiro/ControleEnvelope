from django.shortcuts import render, HttpResponse, redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import EditEnvelopeForm, ProfessorForm, DisciplinaForm, CursoForm, ProvaForm, EnvelopeForm, StatusForm
from .models import Curso, Professor, Status, Secretaria, Prova, Disciplina, Envelope
from django.contrib import messages




from django.forms import modelform_factory #TESTE
import datetime # TESTE
from django.utils.datastructures import MultiValueDictKeyError


def base(request):   #APAGAR
    template_name = 'envelope/base.html'
    context={}
    
    return render(request,template_name , context)

def confirmacao(request, new_form, cursoForm, disciplinaForm):
    template_name = 'envelope/confirmacao_cadastro.html'
    context={
        'curso':cursoForm,
        'disciplina': disciplinaForm,
        'professor': new_form,
    }
    print (cursoForm)
    print (disciplinaForm)
    print (new_form)
    return render(request,template_name , context)

 # APAGAR
def home(request):
    context = {}
    
    template_name = 'envelope/cadastro_professor.html'
    if request.method == 'POST':
        pass
       #return redirect (views.confirmacao(request.POST), new_form , cursoForm, disciplinaForm)
    else:
        messages.info(request,"Erro, verifique os campos!") 
    context= {
        
    }
    return render(request,template_name , context)

#place = models.OneToOneField(Place,on_delete=models.CASCADE,primary_key=True, )
# p1 = Place(name='Demon Dogs', address='944 W. Fullerton')
# p1.save()
# p2 = Place(name='Ace Hardware', address='1013 N. Ashland')
# p2.save()
#Create a Restaurant. Pass the ID of the “parent” object as this object’s ID:

# r = Restaurant(place=p1, serves_hot_dogs=True, serves_pizza=False)
# r.save()

    
# a = ModelA(name="a")
# b = ModelB(name="b")
# a.b = b
# b.save()
# a.save()


def cadastro(request): #ARUMAR ADD CAMPO
    template_name = 'envelope/cadastro.html'
    context={}
    cursos = Curso.objects.all().order_by('nome')
    disciplinas = Disciplina.objects.all().order_by('nome')
    
    professorForm = ProfessorForm()
    cursoForm = CursoForm()
    disciplinaForm = DisciplinaForm()
    if request.method == 'POST':
        professorForm = ProfessorForm(request.POST)
        disciplinaForm = DisciplinaForm(request.POST)
        
        id_curso = request.POST["id_curso"]
        id_disciplina = request.POST["id_disciplina"]
        
        if professorForm.is_valid() and id_curso != '0' and id_disciplina != '0' :
            print ('PASSOU')    # APAGAR
            test_curso = get_object_or_404(Curso, pk=id_curso)
            test_disciplina = get_object_or_404(Disciplina, pk=id_disciplina)
            
            new_prof = professorForm.save(commit = False)
            new_prof.curso = test_curso
            new_prof.disciplina = test_disciplina
            new_prof.save()
            context['success'] = True
            messages.success(request,"Registro "+str(new_prof)+" inserido com sucesso!") #teste
        else:
            messages.info(request,"Erro, verifique os campos!") 
    
    context= {
        'professorForm':professorForm,
        'cursoForm':cursoForm,
        'disciplinaForm':disciplinaForm,
        'cursos': cursos,
        'disciplinas':disciplinas,
    }  
    return render(request,template_name , context)


def confirmacao_cad(request):
    template_name = 'envelope/confirmacao_cadastro.html'
    context={}
    
    return render(request,template_name , context)


def confirmacao_env(request):
    template_name = 'envelope/confirmacao_envelope.html'
    context={}
    
    return render(request,template_name , context)


def details(request, pk):
    template_name = 'envelope/details.html'
    envelope = Envelope.objects.get(envelope_id = pk)
    context={
        'envelope' : envelope,
    }
    return render(request,template_name , context)


def devolucao(request):
    template_name = 'envelope/devolucao.html'
    context={}
    
    return render(request,template_name , context)


def editar_env(request, pk):
    template_name = 'envelope/editar_envelope.html'
    cursos = Curso.objects.all().order_by('nome')
    professores = Professor.objects.all().order_by('nome')
    disciplinas = Disciplina.objects.all().order_by('nome')
    
    envelope = Envelope.objects.get(envelope_id = pk)
    context={
        'envelope':envelope,
        'professores': professores,
        'cursos':cursos,
        'disciplinas':disciplinas,
    }
    
    return render(request,template_name , context)


def envelope(request):
    template_name = 'envelope/envelope.html'
    context={}
    cursos = Curso.objects.all().order_by('nome')
    professores = Professor.objects.all().order_by('nome')
    disciplinas = Disciplina.objects.all().order_by('nome')
    
    cursoForm = CursoForm()
    disciplinaForm = DisciplinaForm()
    provaForm = ProvaForm()
    envelopeForm = EnvelopeForm()
    statusForm = StatusForm()
    
    if request.method == 'POST':
        disciplinaForm = DisciplinaForm(request.POST)
        provaForm = ProvaForm(request.POST)
        cursoForm = CursoForm(request.POST)
        
        id_curso = request.POST["id_curso"]
        id_disciplina = request.POST["id_disciplina"]
        id_professor = request.POST["id_professor"]
        #estagio = request.POST["estagio"]
        #print (provaForm.cleaned_data['data_prova'])
        #print (id_curso)        # APAGAR
        #print (id_professor)    # APAGAR
        print (request.POST)
        print (provaForm.is_valid())
        print (disciplinaForm.is_valid())
        #statusForm.status = '3'
        #print (statusForm.is_valid())
        
        #if id_curso != '0' and id_disciplina != '0' and id_professor != '0' and estagio != '0':
        if provaForm.is_valid() and id_disciplina != '0' and id_professor != '0' and id_curso != '0':
            print("PASSOU")     # APAGAR
            test_curso = get_object_or_404(Curso, pk=id_curso)
            test_disciplina = get_object_or_404(Disciplina, pk=id_disciplina)
            test_professor = get_object_or_404(Professor, pk=id_professor)
            
            #statusForm.save()
            status = statusForm.save(commit = False)
            status.status = 3
            status.save()
            
            new_prova = provaForm.save(commit = False)
            new_prova.status_id = status
            #new_prova.estagio = estagio
            new_prova.save()
            
            new_envelope = envelopeForm.save(commit = False)
            new_envelope.professor = test_professor
            new_envelope.curso = test_curso
            new_envelope.disciplina = test_disciplina
            new_envelope.prova = new_prova
            new_envelope.save()
            context['success'] = True
            messages.success(request,"Registro inserido com sucesso!")
            return redirect('envelope/envelope.html')
        else:
            messages.info(request,"Erro, verifique os campos!")
    
    context = {
        'professores': professores,
        'cursos':cursos,
        'disciplinas':disciplinas,
        'cursoForm': cursoForm,
        'disciplinaForm' : disciplinaForm,
        'provaForm':provaForm,
        'envelopeForm':envelopeForm,
    }
    return render(request,template_name , context)


def gerarrelatorio(request):
    template_name = 'envelope/gerar_relatorio.html'
    estagio='a'
    status=''
    if( request.method == 'POST'):
        estagio = request.POST["estagio"]
        status = request.POST["status"]
        print(estagio)
        print(status)
        context={
            'estagio':estagio,
            'status':status,
        }
    else:
        context={}
    
    return render(request,template_name , context)


def gerenciarenvelope(request):
    template_name = 'envelope/gerenciarenvelope.html'
    context={}
    professores = Professor.objects.all().order_by('nome')
    cursos = Curso.objects.all().order_by('nome')
    disciplinas = Disciplina.objects.all().order_by('nome')
    prova = Prova.objects.all().order_by('data_prova')
    
    cursoForm = CursoForm()
    disciplinaForm = DisciplinaForm()
    context = {
        'professores':professores,
        'cursos':cursos,
        'disciplinas':disciplinas,
        'cursoForm':cursoForm,
        'disciplinaForm' : disciplinaForm,
    }
    return render(request,template_name , context)

def index(request):
    template_name = 'envelope/index.html'
    context={}
    envelopes = Envelope.objects.all()
    #envelope = request["a"] # TESTE
    #print(envelope.id)
    if (envelopes):
        context = {
        'envelopes' : envelopes,
    }    
    else:
        messages.info(request,"Não possui envelope cadastrado!")
    
    return render(request,template_name , context)


def listastatus(request):
    template_name = 'envelope/listastatus.html'
    context={}
    
    return render(request,template_name , context)


def login(request):
    template_name = 'envelope/login.html'
    context={}
    
    return render(request,template_name , context)


def redefine(request):
    template_name = 'envelope/redefine.html'
    context={}
    
    return render(request,template_name , context)


def relatorio(request):
    template_name = 'envelope/relatorio.html'
    context={}
    
    print(estagio)
    return render(request,template_name , context)

