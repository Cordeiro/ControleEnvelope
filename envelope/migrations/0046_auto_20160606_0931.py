# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-06 12:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('envelope', '0045_auto_20160602_0244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='data_devolucao',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Entregue em'),
        ),
    ]
