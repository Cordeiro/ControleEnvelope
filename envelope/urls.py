from django.conf.urls import url, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^test/$', views.home), # APAGAR
    url(r'^base/$', views.base), # APAGAR
    url(r'^index/$', views.index),
    url(r'^confirmacao/$', views.confirmacao),
    url(r'^cadastro/$', views.cadastro),
    url(r'^confirmacao_cad/$', views.confirmacao_cad),
    url(r'^confirmacao_env/$', views.confirmacao_env),
    url(r'^details/(?P<pk>[0-9]+)/$', views.details),
    url(r'^devolucao/$', views.devolucao),
    url(r'^editar_env/(?P<pk>[0-9]+)/$', views.editar_env),
    url(r'^envelope/$', views.envelope),
    url(r'^gerar_relatorio/$', views.gerarrelatorio),
    url(r'^gerenciarenvelope/$', views.gerenciarenvelope),
    url(r'^listastatus/$', views.listastatus),
    url(r'^$', 'django.contrib.auth.views.login', 
    	{'template_name': 'envelope/login.html'}),
    url(r'^redefine/$', views.redefine),
    url(r'^relatorio/$', views.relatorio),
]