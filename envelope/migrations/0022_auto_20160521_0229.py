# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-21 05:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('envelope', '0021_auto_20160521_0123'),
    ]

    operations = [
        migrations.CreateModel(
            name='Disciplina_Curso',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('curso', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='envelope.Curso')),
            ],
        ),
        migrations.CreateModel(
            name='Disciplina_Professor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.AddField(
            model_name='professor',
            name='curso',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='envelope.Curso'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='professor',
            name='disciplina',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='envelope.Disciplina'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='disciplina',
            name='prova',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='envelope.Prova'),
        ),
        migrations.AlterField(
            model_name='professor',
            name='matricula',
            field=models.CharField(max_length=15, unique=True, verbose_name='Matricula'),
        ),
        migrations.AlterField(
            model_name='professor',
            name='nome',
            field=models.CharField(max_length=80, unique=True, verbose_name='Nome'),
        ),
        migrations.AddField(
            model_name='disciplina_professor',
            name='disciplina',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='envelope.Disciplina'),
        ),
        migrations.AddField(
            model_name='disciplina_professor',
            name='professor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='envelope.Professor'),
        ),
        migrations.AddField(
            model_name='disciplina_curso',
            name='disciplina',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='envelope.Disciplina'),
        ),
    ]
