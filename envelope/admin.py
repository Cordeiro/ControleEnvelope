from django.contrib import admin
from .models import Curso, Professor, Status, Secretaria, Prova, Disciplina, Envelope

admin.site.register(Curso)
admin.site.register(Professor)
admin.site.register(Status)
admin.site.register(Secretaria)
admin.site.register(Prova)
admin.site.register(Disciplina)
admin.site.register(Envelope)
