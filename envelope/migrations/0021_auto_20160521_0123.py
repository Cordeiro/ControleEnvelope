# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-21 04:23
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('envelope', '0020_auto_20160520_1231'),
    ]

    operations = [
        migrations.RenameField(
            model_name='disciplina',
            old_name='prova_id',
            new_name='prova',
        ),
    ]
