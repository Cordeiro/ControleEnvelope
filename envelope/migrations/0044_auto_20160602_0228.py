# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-02 05:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('envelope', '0043_auto_20160602_0159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prova',
            name='data_prova',
            field=models.DateTimeField(verbose_name='Data da Prova'),
        ),
    ]
